CREATE USER 'yourUsername'@'%' IDENTIFIED BY 'yourPassword';
ALTER USER 'yourUsername'@'%' IDENTIFIED WITH mysql_native_password BY 'yourPassword';
GRANT ALL PRIVILEGES ON customer_data.* TO 'yourUsername'@'%';
FLUSH PRIVILEGES;
