# Verwenden Sie das offizielle Node.js-Image als Basis
FROM node:16

# Setzen Sie das Arbeitsverzeichnis im Container
WORKDIR /usr/src/app

# Kopieren Sie die package.json (und package-lock.json, falls vorhanden) in das Arbeitsverzeichnis
COPY package*.json ./

# Installieren Sie die Projektabhängigkeiten
RUN npm install

# Kopieren Sie den Rest der Anwendungsquellen in das Arbeitsverzeichnis
COPY . .

# Ihr Anwendung sollte auf einem bestimmten Port laufen
EXPOSE 8080

# Definieren Sie den Befehl, der beim Start des Containers ausgeführt wird
CMD ["node", "app.js"]
