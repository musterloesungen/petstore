'use strict';

const db = require('./db');

/**
 * Create an owner
 *
 * body Owner
 * returns Owner
 **/
exports.createOwner = function(body) {
  return db.query('INSERT INTO Owners (first_name, last_name, email) VALUES (?, ?, ?)', [body.first_name, body.last_name, body.email])
    .then(([result]) => ({
      owner_id: result.insertId,
      first_name: body.first_name,
      last_name: body.last_name,
      email: body.email
    }))
    .catch(err => {
      throw new Error('Database error: ' + err.message);
    });
};



/**
 * Create an owner and a pet, and associate the pet with the owner
 *
 * body Owners_and_pets_body
 * returns owners_and_pets_body
 **/
/**
 * Create multiple owners and their pets from a list
 *
 * body Owners_and_pets_body_list
 * returns Owners_and_pets_body_list
 **/
exports.createOwnerAndPet = function(body) {
  return db.getConnection().then(conn => {
    return conn.beginTransaction().then(() => {
      const ownerPromises = body.map(ownerAndPets => {
        return conn.query('INSERT INTO Owners (first_name, last_name, email) VALUES (?, ?, ?)',
          [ownerAndPets.first_name, ownerAndPets.last_name, ownerAndPets.email])
          .then(([ownerResult]) => {
            const petPromises = ownerAndPets.pets.map(pet => {
              return conn.query('INSERT INTO Pets (pet_name, species, fk_owned_by) VALUES (?, ?, ?)',
                [pet.name, pet.species, ownerResult.insertId]);
            });
            return Promise.all(petPromises).then(petResults => {
              return {
                owner_id: ownerResult.insertId,
                first_name: ownerAndPets.first_name,
                last_name: ownerAndPets.last_name,
                email: ownerAndPets.email,
                pets: petResults.map(([petResult], index) => {
                  return {
                    pet_id: petResult.insertId,
                    name: ownerAndPets.pets[index].name,
                    species: ownerAndPets.pets[index].species,
                    fk_owned_by: ownerResult.insertId
                  };
                })
              };
            });
          });
      });

      return Promise.all(ownerPromises).then(results => {
        return conn.commit().then(() => results);  // Always returns an array of results
      });
    }).catch(err => {
      return conn.rollback().then(() => {
        throw new Error('Transaction failed: ' + err.message);
      });
    });
  });
};



/**
 * Create a pet
 *
 * body Pet
 * returns Pet
 **/
exports.createPet = function(body) {
  return db.query('INSERT INTO Pets (pet_name, species, fk_owned_by) VALUES (?, ?, ?)', [body.pet_name, body.species, body.fk_owned_by])
    .then(([result]) => ({
      pet_id: result.insertId,
      pet_name: body.pet_name,
      species: body.species,
      fk_owned_by: body.fk_owned_by
    }))
    .catch(err => {
      throw new Error('Database error: ' + err.message);
    });
};

/**
 * Get all information about one or more owners
 *
 * returns List
 **/
exports.getOwners = function() {
  return db.query('SELECT * FROM Owners')
    .then(([rows]) => rows)
    .catch(err => {
      throw new Error('Database error: ' + err.message);
    });
};



/**
 * Get all information about one or more pets
 *
 * returns List
 **/
exports.getPets = function() {
  return db.query('SELECT * FROM Pets')
    .then(([rows]) => rows)
    .catch(err => {
      throw new Error('Database error: ' + err.message);
    });
};



/**
 * Get all pets associated with one owner
 *
 * ownerId Integer
 * returns List
 **/
exports.getPetsByOwner = function(ownerId) {
  return db.query('SELECT * FROM Pets WHERE fk_owned_by = ?', [ownerId])
    .then(([rows]) => rows)
    .catch(err => {
      throw new Error('Database error: ' + err.message);
    });
};


